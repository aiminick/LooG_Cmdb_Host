 **CMDB系统开发：<br/>** 
  
   目前完成的功能：基于cobbler的二次开发，方便不会的机房和运维人员快速装机，
CMDB的快速添加，CMDB机柜平台，机柜和故障告警展示，,zabbix 的模板管理，把CMDB同步到zabbix 数据库，然后批量绑定模板，删除维护周期，zabbix调用图片运维平台生成图形；
   后期会基于walle系统概念开发出 代码发布平台,基于蓝鲸系统的 批量管理平台。

一、装机平台：基于cobbler来做二次开发. <br/>

平台指定安装操作系统版本镜像，安装的分区规划。当然定制ks文件这一块可以随机修改。
通过厂商MAC地址，指定IP,MAC,网关等，通过管理平台指定IP和系统版本之后，机房相关人员插上网线开机即可安装。
记录操作数据。
![输入图片说明](https://gitee.com/uploads/images/2018/0304/154349_da6abc8a_1515764.png "装机定制.png")

二、用户权限管理：<br/>

可以对用户进行管理，对用户增删改查修改密码等。
用户权限管理，比如监控组有zabbix操作权限，故障申报所有人都有权限，故障下架处理由管理员执行
![输入图片说明](https://gitee.com/uploads/images/2018/0304/161410_629b1f23_1515764.png "权限管理.png")


三、CMDB管理：<br/>

机房，机柜的相关管理增删改查，这一块只有admin用户可以查看和修改。
脚本采集系统硬件数据，通过API方式提交和人工录入的半自动方式。
数据收集参数和删除。
![输入图片说明](https://gitee.com/uploads/images/2018/0305/201430_78960190_1515764.png "监控展示.png")

四、zabbix管理：<br/>

通过CMDB平台联动到zabbix数据库。
实现对zabbix主机的批量模板绑定、删除；基本上已经完成zabbix管理工作。
主机出现问题，或者维护数据时候添加维护周期。
调用zabbix图形在运维平台展示
![输入图片说明](https://gitee.com/uploads/images/2018/0304/154539_3c0c6107_1515764.png "主机绑定Zabbix 模板.png")
![输入图片说明](https://gitee.com/uploads/images/2018/0304/155055_232d8de0_1515764.png "添加维护周期.png")

五、系统保障处理: <br/>

工程师把处理的故障上报平台，管理员，项目经理知悉；并且异步发送邮件到管理员有些，管理员给出操作指示，下架等操作
![输入图片说明](https://gitee.com/uploads/images/2018/0304/155002_dae21036_1515764.png "故障处理.png")

六、机柜机房拓扑展示：<br/>

根据录入的主机机房，机柜信息，进行拓扑展示。告警结合zabbix API动态展示机器状态
  
![输入图片说明](https://gitee.com/uploads/images/2018/0304/160918_7642b7b4_1515764.png "机柜位置.png")